﻿using BP.Rest.Api.Helpers.Email;
using Xunit;

namespace BP.Rest.Api.Test.API.Helpers
{
    public class EmailHelperTests
    {
        public EmailHelperTests()
        {
            _emailHelper = new EmailHelper();
        }

        private readonly IEmailHelper _emailHelper;

        [Fact]
        public void GivenAnInvalidEmail_WhenValidate_ShouldReturnFalse()
        {
            //Arrange
            var invalidEmail1 = "name.co.uk";
            var invalidEmail2 = "name";
            var invalidEmail3 = "name@something";

            //Act
            var result1 = _emailHelper.IsValidEmail(invalidEmail1);
            var result2 = _emailHelper.IsValidEmail(invalidEmail2);
            var result3 = _emailHelper.IsValidEmail(invalidEmail3);

            //Assert
            Assert.False(result1);
            Assert.False(result2);
            Assert.False(result3);
        }

        [Fact]
        public void GivenAnInvalidPassword_WhenValidate_ShouldReturnFalse()
        {
            /* 
             Password rules
             1) It must contain at least a number
             2) one upper case letter
             3) 8 characters long.
            */

            //Arrange
            var invalidPassword1 = "12345"; // <8 charactors
            var invalidPassword2 = "12edi@1234"; // No uppercase letter
            var invalidPassword3 = "welcome"; // No number

            //Act
            var result1 = _emailHelper.IsValidPassword(invalidPassword1);
            var result2 = _emailHelper.IsValidPassword(invalidPassword2);
            var result3 = _emailHelper.IsValidPassword(invalidPassword3);

            //Assert
            Assert.False(result1);
            Assert.False(result2);
            Assert.False(result3);
        }

        [Fact]
        public void GivenAValidEmail_WhenValidate_ShouldReturnTrue()
        {
            //Arrange
            var validEmail = "name@auden.co.uk";

            //Act
            var result = _emailHelper.IsValidEmail(validEmail);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void GivenAVlidPassword_WhenValidate_ShouldReturnTrue()
        {
            /* 
             Password rules
             1) It must contain at least a number
             2) one upper case letter
             3) 8 characters long.
            */

            //Arrange
            var validPassword1 = "1Abcdefgh";

            //Act
            var result = _emailHelper.IsValidPassword(validPassword1);

            //Assert
            Assert.True(result);
        }
    }
}