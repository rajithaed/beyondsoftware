﻿using System.Collections.Generic;
using BP.Rest.Api.Models.DomainModels;
using BP.Rest.Api.Repositories;
using Moq;
using Xunit;

namespace BP.Rest.Api.Test.API.Repositories
{
    public class UserRepositoryTests
    {
        public UserRepositoryTests()
        {
            _userRepository = new UserRepository();
            var testMoq = new Mock<IUserRepository>();
        }

        private readonly IUserRepository _userRepository;


        [Fact]
        public void GivenAUserId_WhenGetByIdIsCalled_ThenReturnUser()
        {
            //Arrange
            var Id = 2;

            //Act
            var result = _userRepository.GetById(Id);

            //Assert
            var expected = typeof(User);

            Assert.IsType<User>(result);
            Assert.IsType(expected, result);
        }

        [Fact]
        public void GivenAUserRepository_WhenGetAllIsCalled_ThenReturnAllUsers()
        {
            //Arrange


            //Act
            var result = _userRepository.GetAll();

            //Assert
            var expected = typeof(List<User>);

            Assert.IsType<List<User>>(result);
            Assert.IsType(expected, result);
        }
    }
}