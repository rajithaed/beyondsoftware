﻿using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace BP.Rest.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseStartup<Startup>()
                .UseApplicationInsights()
                .UseIISIntegration()
                .Build();

            host.Run();
        }
    }
}