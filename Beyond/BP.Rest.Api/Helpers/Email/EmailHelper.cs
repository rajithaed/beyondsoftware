using System.Text.RegularExpressions;

namespace BP.Rest.Api.Helpers.Email
{
    public class EmailHelper : IEmailHelper
    {
        public bool IsValidPassword(string password)
        {
            //This is a just a Mock to simulate the implementation

            var hasNumber = new Regex(@"[0-9]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var hasMinimum8Chars = new Regex(@".{8,}");

            return hasNumber.IsMatch(password)
                   && hasUpperChar.IsMatch(password)
                   && hasMinimum8Chars.IsMatch(password);
        }

        public bool IsValidEmail(string email)
        {
            //todo: integrate with DocMail for implementation
            //This is a just a Mock to simulate the implementation

            const string emailRegEx =
                @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";
            return Regex.IsMatch(email, emailRegEx, RegexOptions.IgnoreCase);
        }
    }
}