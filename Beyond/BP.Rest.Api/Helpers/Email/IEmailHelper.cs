namespace BP.Rest.Api.Helpers.Email
{
    public interface IEmailHelper
    {
        bool IsValidPassword(string password);
        bool IsValidEmail(string email);
    }
}