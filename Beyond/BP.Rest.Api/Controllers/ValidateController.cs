﻿using BP.Rest.Api.Helpers.Email;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace BP.Rest.Api.Controllers
{
    [Route("api/Validate")]
    public class ValidateController : Controller
    {
        private readonly IEmailHelper _emailHelper;

        public ValidateController(IEmailHelper emailHelper)
        {
            _emailHelper = emailHelper;
        }

        [HttpGet]
        [Route("email/{*emailString}")]
        [AllowAnonymous]
        public IActionResult ValidateEmail(string emailString)
        {
            var result = _emailHelper.IsValidEmail(emailString);

            return Content(JsonConvert.SerializeObject(result));
        }

        [HttpGet]
        [Route("password/{*passwordString}")]
        [AllowAnonymous]
        public IActionResult ValidatePassword(string passwordString)
        {
            var result = _emailHelper.IsValidPassword(passwordString);

            return Content(JsonConvert.SerializeObject(result));
        }
    }
}