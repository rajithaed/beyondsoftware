using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using BP.Rest.Api.Models.DomainModels;

namespace BP.Rest.Api.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly string _connectionString;

        public UserRepository()
        {
            //todo: move this to a configuration setting either in web.config or config.json 
            _connectionString = @"Server=bp;Database=bp;Trusted_Connection=True;";
        }

        public IDbConnection Connection => new SqlConnection(_connectionString);

        public User GetById(int id)
        {
            //todo:need implementation
            return new User();
        }

        public void Add(User newUser)
        {
            throw new NotImplementedException();
        }

        public List<User> GetAll()
        {
            throw new NotImplementedException();
        }

        public void Update(User user)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}