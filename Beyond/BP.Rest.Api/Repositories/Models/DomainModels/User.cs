namespace BP.Rest.Api.Models.DomainModels
{
    public class User
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}