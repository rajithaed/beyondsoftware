﻿using System.Collections.Generic;
using BP.Rest.Api.Models.DomainModels;

namespace BP.Rest.Api.Repositories
{
    public interface IUserRepository
    {
        User GetById(int id);
        void Add(User newUser);
        List<User> GetAll();
        void Update(User user);
        void Delete(int id);
    }
}